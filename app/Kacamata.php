<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Kacamata extends Model
{
    //`id`, `title`, `desc`, `harga`, `hargadummy`, `deleted_at`, `created_at`, `updated_at`
    
    protected $table = 'kacamata';

    protected $fillable = ['title','desc', 'harga', 'hargadummy' ];

	protected $guarded = ["id"]; 
	public $timestamps = true;
}
