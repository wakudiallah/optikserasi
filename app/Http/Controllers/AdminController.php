<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Model\Dease;
use App\Model\DetailSick;
use App\Model\History;
use App\Model\Role;
use App\Model\DetailOptik;
use DB;
use Session;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

    public function __construct()
    {
        $this->middleware('auth');
    }


    public function index()
    {
         $user  = User::where('role_id','<>','3')->get();

        return view('ojek.admin.index',compact('role','user'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $user  = User::where('role_id','<>','3')->get();
        $role  = Role::where('id','<>','3')->get();

        return view('ojek.admin.add_pengguna',compact('role','user'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $name     = $request->input('name');
        $email    = $request->input('email');
        $password = $request->input('password');
        $phone    = $request->input('phone');
        $akses    = $request->input('akses');

        $request           = new User;
        $request->name     = $name;
        $request->email    = $email;
        $request->password = Hash::make($password);
        $request->password_real = $password;
        $request->phone    = $phone;
        $request->role_id  = $akses;
        $request->save();

        return redirect('/admin-index');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
