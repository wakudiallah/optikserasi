<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Kacamata;

use App\Authorizable;

class KacamataController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kacamata = Kacamata::get();

        return view('ojek.kacamata.index', compact('kacamata'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('ojek.kacamata.new');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $title      = $request->input('title');
        $desc       = $request->input('desc');
        $hargadummy = $request->input('hargadummy');
        
        $harga     = $request->input('harga');
        $gambar     = $request->input('gambar');

        if($request->hasFile('gambar')) {
            $file = $request->file('gambar');
            $date = date('Y-m-d');
            $extension = $file->getClientOriginalExtension();
            $file_name = $gambar. '.' . $extension;
            $destinationPath = 'kacamata';
            $file->move($destinationPath, $file_name );
        }
        
        $upload_file = $file_name;

        $kc             = new Kacamata;
        $kc->title      = $title;
        $kc->desc       = $desc;
        $kc->harga = $harga;
        $kc->hargadummy = $hargadummy;
        $kc->gambar     = $upload_file;
        $kc->save();

        return redirect('kacamata-admin')->with(['update' => 'Data Kacamata sukses di simpan']);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
