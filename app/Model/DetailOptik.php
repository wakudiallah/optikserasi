<?php

namespace App\Model;


use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class DetailOptik extends Model
{
    //`id`, `title`, `desc`, `harga`, `hargadummy`, `deleted_at`, `created_at`, `updated_at`
    
    protected $table = 'detail_optik';

    

	protected $guarded = ["id"]; 
	public $timestamps = true;
}
