<?php

namespace App\Model;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use DB;

class Role extends Model
{
    //`id`, `title`, `desc`, `harga`, `hargadummy`, `deleted_at`, `created_at`, `updated_at`
    
    protected $table = 'role';

    

	protected $guarded = ["id"]; 
	public $timestamps = true;
}
