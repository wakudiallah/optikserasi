@extends('layout.admin')

@section('content')

	<!-- breadcumber -->
	<section class="content-header">
      <h1>
        Product
        <small>Kacamata</small>
      </h1>
      <ol class="breadcrumb">
        <li><a href="#"><i class="fa fa-dashboard"></i> Product</a></li>
        <li class="active">Kacamata</li>
      </ol>
    </section>
    <!-- end breadcumber -->

    <section class="content">

    	<div class="row">
    		<div class="col-md-10"></div>
    		<div class="col-md-2">
    			<a class="btn btn-block btn-primary" href="{{route('kacamata-admin.create')}}">Tambah</a>
    		</div>	
    	</div>

    	<div class="row">
    		<div class="col-md-12">
    			<div class="box">
		            
		            <!-- /.box-header -->
		            <div class="box-body">
		              <table id="example" class="table table-bordered table-striped">
		                <thead>
		                <tr>
		                  <th>ID</th>
		                  <th>Title</th>
		                  <th>Desc</th>
		                  <th>Harga</th>
		                  <th>Harga Strip</th>
		                  <th>Gambar</th>
		                  <th>Action</th>
		                </tr>
		                </thead>
		                <tbody>
		                	@foreach($kacamata as $data)
		                <tr>
		                  <td></td>
		                  <td>{{$data->title}}</td>
		                  <td>{{$data->desc}}</td>
		                  <td> {{$data->harga}}</td>
		                  <td>{{$data->hargadummy}}</td>
		                  <td><img src="{{url('/')}}kacamata/{{$data->gambar}}"></td>


		                  	<td></td>
		                </tr>
		                @endforeach
		                
		                </tfoot>
		              </table>
		            </div>
		            <!-- /.box-body -->
		          </div>
		          <!-- /.box -->
    		</div>
    	</div>
    	
    </section>


    <script type="text/javascript" src="https://code.jquery.com/jquery-3.3.1.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/dataTables.buttons.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.flash.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/pdfmake.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.32/vfs_fonts.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.html5.min.js"></script>
	<script type="text/javascript" src="https://cdn.datatables.net/buttons/1.5.1/js/buttons.print.min.js"></script>
	
	<script>
	    $(document).ready(function() {
	    $('#example').DataTable();
	    });
	</script>

	<script>
	    $(document).ready(function() {
	    $('#example1').DataTable( {
	        dom: 'Bfrtip',
	        buttons: [
	            'copyHtml5',
	            'excelHtml5',
	            'csvHtml5',
	            'pdfHtml5'
	        ]
	    } );
	} );
	</script>

@endsection