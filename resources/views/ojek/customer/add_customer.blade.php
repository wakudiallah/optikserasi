@extends('ojek.layout_admin.template')
@section('content')


  <section class="content-header">
      <h1>Tambah Customer </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('ojek')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="{{route('customer.index')}}">Customer</a></li>
        <li><a href="#">Tambah Customer</a></li>
      </ol>
    </section>


  <div class="content">
    <div class="box box-info">
            <div class="box-header with-border">
              <h3 class="box-title">Tambah Customer</h3>
            </div>
            <!-- /.box-header -->
            <!-- form start -->
            <form class="form-horizontal" id="myform">
              <div class="box-body">
                <div class="form-group">
                  <label  class="col-sm-2 control-label">ID Customer</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" name="id_customer" placeholder="ID Customer" onkeyup="this.value = this.value.toUpperCase()" required>
                  </div>
                </div>

                <div class="form-group">
                  <label  class="col-sm-2 control-label">Nama</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputEmail3" placeholder="Nama" name="name" onkeyup="this.value = this.value.toUpperCase()" required>
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputEmail3" class="col-sm-2 control-label">Email</label>

                  <div class="col-sm-10">
                    <input type="email" class="form-control" id="inputEmail3" placeholder="Email" name="email" required>
                  </div>
                </div>
                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Alamat</label>

                  <div class="col-sm-10">
                    <textarea class="form-control" rows="3" placeholder="Alamat" name="address" onkeyup="this.value = this.value.toUpperCase()" required id="searchMapInput"></textarea>
                  </div>
                </div>


                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Hp</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputPassword3" placeholder="Hp" name="phone">
                  </div>
                </div>


                <div class="form-group">
                <label for="inputPassword3" class="col-sm-2 control-label">Date</label>

                <div class="col-sm-10">
                <div class="input-group date">
                  <div class="input-group-addon">
                    <i class="fa fa-calendar"></i>
                  </div>
                  <input type="text" class="form-control pull-right" id="datepicker">
                </div>
                </div>
                <!-- /.input group -->
              </div>

              <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Perusahaan</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputPassword3" placeholder="Perusahaan" name="employment">
                  </div>
                </div>

                <div class="form-group">
                  <label for="inputPassword3" class="col-sm-2 control-label">Jabatan</label>

                  <div class="col-sm-10">
                    <input type="text" class="form-control" id="inputPassword3" placeholder="Jabatan" name="position">
                  </div>
                </div>

                

                <!-- <input id="searchMapInput" class="mapControls" type="text" placeholder="Enter a location"> -->



                <div class="form-group">
                  <div class="col-sm-offset-2 col-sm-10">
                    <div class="checkbox">
                      <label>
                        <input type="checkbox"> Remember me
                      </label>
                    </div>
                  </div>
                </div>
              </div>
              <!-- /.box-body -->
              <div class="box-footer">
                
                <button type="submit" class="btn btn-info pull-right">Save</button>
              </div>
              <!-- /.box-footer -->
            </form>
          </div>
    </div>



@endsection


@push('custom-scripts')
    <script type="text/javascript">
      $(document).ready(function () {

        $('#myform').validate({ // initialize the plugin
            rules: {
                id_customer: {
                    required: true
                },
                name: {
                    required: true,
                    maxlength: 100
                },
                email: {
                    required: true
                },
                address: {
                    required: true
                },
                phone: {
                    required: true
                }

            },
            submitHandler: function (form) { // for demo
                alert('valid form submitted'); // for demo
                return false; // for demo
            }
        });

    });
    </script>


   <script>
    function initMap() {
        var input = document.getElementById('searchMapInput');
      
        var autocomplete = new google.maps.places.Autocomplete(input);
       
        autocomplete.addListener('place_changed', function() {
            var place = autocomplete.getPlace();
            document.getElementById('location-snap').innerHTML = place.formatted_address;
            document.getElementById('lat-span').innerHTML = place.geometry.location.lat();
            document.getElementById('lon-span').innerHTML = place.geometry.location.lng();
        });
    }
    </script>
    
    
     <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyATzsafy5WHjV4871j-qZmJYfZXXlrxSv0&libraries=places&callback=initMap"
        async defer></script>
@endpush