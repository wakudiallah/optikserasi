@extends('ojek.layout_admin.template')
@section('content')
	<section class="content-header">
      <h1> Customer </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('ojek')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Customer</a></li>
      </ol>
    </section>


	<div class="content">
		<a href="{{route('customer.create')}}" class="btn btn-flat btn-primary">Tambah</a>
		<div class="box">
            <div class="box-header">
              <h3 class="box-title">Customer</h3>
            </div>
            <!-- /.box-header -->
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>ID</th>
                  <th>Nama</th>
                  <th>Alamat</th>
                  <th>Hp</th>
                  <th>Perusahaan</th>
                  <th>Riwayat</th>
                  <th>Detail</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                	@foreach($user as $data)
                <tr>
                  <td></td>
                  <td></td>
                  <td>{{$data->name}}</td>
                  <td>{{$data->address}}</td>
                  <td>{{$data->phone}}</td>
                  <td>{{$data->employment}}</td>
                  <td></td>
                  <td></td>
                  <td></td>
                </tr>
                
                
                	@endforeach
                
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
    </div>

@endsection