@extends('ojek.layout_admin.template')
@section('content')

	<section class="content-header">
      <h1> Pengguna </h1>
      <ol class="breadcrumb">
        <li><a href="{{url('ojek')}}"><i class="fa fa-dashboard"></i> Home</a></li>
        <li><a href="#">Pengguna</a></li>
      </ol>
    </section>

	<div class="content">
		<a href="{{url('admin-create')}}" class="btn btn-flat btn-primary">Tambah</a>
		<div class="box">
            <div class="box-header">
              <h3 class="box-title">Pengguna</h3>
            </div>
            
            <div class="box-body">
              <table id="example1" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th>No</th>
                  <th>Nama</th>
                  <th>Email</th>
                  <th>Hp</th>
                  <th>Akses</th>
                  <th>Aksi</th>
                </tr>
                </thead>
                <tbody>
                  <?php $i = 1; ?>
                @foreach($user as $data)
                <tr>
                  <td>{{$i++}}</td>
                  <td>{{$data->name}}</td>
                  <td>{{$data->email}}</td>
                  <td>{{$data->phone}}</td>
                  <td></td>
                  <td></td>
                </tr>
                @endforeach
                
                
                </tbody>
                
              </table>
            </div>
            <!-- /.box-body -->
          </div>
          <!-- /.box -->
        </div>
    </div>

@endsection