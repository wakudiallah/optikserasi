@extends('layout.utama')

@section('content')

	<section class="slider-wrap site-section">
      <div class="container">
        <div class="row">
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('images/img_2.jpg')">
              <div class="text">
                <span class="icon ion-ios-videocam"></span>
                <h2>Video in Stockton Beach, Austrlia</h2>
                <p>Continue reading...</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('images/img_1.jpg')">
              <div class="text">
                <span class="icon ion-images"></span>
                <h2>Photography in Trogir, Croatia</h2>
                <p>Continue reading...</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg last" style="background-image: url('images/img_3.jpg')">
              <div class="text">
                <span class="icon ion-document"></span>
                <h2>Enjoyimg the Desert in Morocco</h2>
                <p>Continue reading...</p>
              </div>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('images/img_4.jpg')">
              <div class="text">
                <span class="icon ion-images"></span>
                <h2>Travel in India's Popular Places</h2>
                <p>Continue reading...</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('images/img_5.jpg')">
              <div class="text">
                <span class="icon ion-document"></span>
                <h2>Popular Tower in France</h2>
                <p>Continue reading...</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg last" style="background-image: url('images/img_6.jpg')">
              <div class="text">
                <span class="icon ion-document"></span>
                <h2>Best Places in Australia</h2>
                <p>Continue reading...</p>
              </div>
            </a>
          </div>
        </div>

        <div class="row">
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('asset/images/img_2.jpg')">
              <div class="text">
                <span class="icon ion-ios-videocam"></span>
                <h2>Video in Stockton Beach, Austrlia</h2>
                <p>Continue reading...</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('asset/images/img_3.jpg')">
              <div class="text">
                <span class="icon ion-images"></span>
                <h2>Photography in Trogir, Croatia</h2>
                <p>Continue reading...</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg last" style="background-image: url('asset/images/img_3.jpg')">
              <div class="text">
                <span class="icon ion-document"></span>
                <h2>Enjoyimg the Desert in Morocco</h2>
                <p>Continue reading...</p>
              </div>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('asset/images/img_3.jpg')">
              <div class="text">
                <span class="icon ion-images"></span>
                <h2>Travel in India's Popular Places</h2>
                <p>Continue reading...</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('asset/images/img_2.jpg')">
              <div class="text">
                <span class="icon ion-document"></span>
                <h2>Popular Tower in France</h2>
                <p>Continue reading...</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg last" style="background-image: url('asset/images/img_2.jpg)">
              <div class="text">
                <span class="icon ion-document"></span>
                <h2>Best Places in Australia</h2>
                <p>Continue reading...</p>
              </div>
            </a>
          </div>
        </div>

        <div class="row element-animate pt-5">
          <div class="col-6 text-left mb-3"><a href="#" class="btn btn-primary">Prev</a></div>
          <div class="col-6 text-right"><a href="#" class="btn btn-primary">Next</a></div>
        </div>
      </div>
    </section>
    <!-- END section -->



	<section class="section-cover" data-stellar-background-ratio="0.5" style="background-image: url(asset/images/study-book.jpg);">
      <div class="container">
        <div class="row justify-content-center align-items-center intro">
          <div class="col-md-7 text-center element-animate">
            <h2>Kami selalu memberikan pelayanan dan penawaran terbaik</h2>
            
            <p><a href="#" class="btn btn-black">Cek Produk terbaru</a></p>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

@endsection