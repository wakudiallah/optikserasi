@extends('layout.utama')

@section('content')

	<!-- <section class="feature-destination">
      <div class="container">
        <div class="row">
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('asset/images/home.jpg')">
              <div class="text">
                <span class="icon ion-ios-location"></span>
                <h2>Stockton Beach, Austrlia</h2>
                <p>Visit This Place</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('asset/images/home2.jpg')">
              <div class="text">
                <span class="icon ion-ios-location"></span>
                <h2>Trogir, Croatia</h2>
                <p>Visit This Place</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg last" style="background-image: url('asset/images/barcalona-biker.jpg')">
              <div class="text">
                <span class="icon ion-ios-location"></span>
                <h2>Desert, Morocco</h2>
                <p>Visit This Place</p>
              </div>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('asset/images/home.jpg')">
              <div class="text">
                <span class="icon ion-ios-location"></span>
                <h2>Stockton Beach, Austrlia</h2>
                <p>Visit This Place</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('asset/images/home2.jpg')">
              <div class="text">
                <span class="icon ion-ios-location"></span>
                <h2>Trogir, Croatia</h2>
                <p>Visit This Place</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg last" style="background-image: url('asset/images/barcalona-biker.jpg')">
              <div class="text">
                <span class="icon ion-ios-location"></span>
                <h2>Desert, Morocco</h2>
                <p>Visit This Place</p>
              </div>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('asset/images/home.jpg')">
              <div class="text">
                <span class="icon ion-ios-location"></span>
                <h2>Stockton Beach, Austrlia</h2>
                <p>Visit This Place</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('asset/images/home2.jpg')">
              <div class="text">
                <span class="icon ion-ios-location"></span>
                <h2>Trogir, Croatia</h2>
                <p>Visit This Place</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg last" style="background-image: url('asset/images/barcalona-biker.jpg')">
              <div class="text">
                <span class="icon ion-ios-location"></span>
                <h2>Desert, Morocco</h2>
                <p>Visit This Place</p>
              </div>
            </a>
          </div>
        </div>
        <div class="row">
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('asset/images/home.jpg')">
              <div class="text">
                <span class="icon ion-ios-location"></span>
                <h2>Stockton Beach, Austrlia</h2>
                <p>Visit This Place</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg" style="background-image: url('asset/images/home2.jpg')">
              <div class="text">
                <span class="icon ion-ios-location"></span>
                <h2>Trogir, Croatia</h2>
                <p>Visit This Place</p>
              </div>
            </a>
          </div>
          <div class="col-md-4 element-animate ">
            <a href="#" class="img-bg last" style="background-image: url('asset/images/barcalona-biker.jpg')">
              <div class="text">
                <span class="icon ion-ios-location"></span>
                <h2>Desert, Morocco</h2>
                <p>Visit This Place</p>
              </div>
            </a>
          </div>
        </div>
      </div>
    </section> -->
    <!-- END section -->

    <section class="site-section">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
            <h2>Kacamata</h2>
          </div>
        </div>
        <div class="row top-destination">
          <div class="col-lg-4 col-md-4 col-sm-6 col-12">
            <a href="#" class="place">
              <img src="asset/images/img_1.jpg" alt="Image placeholder">
              <h2>Trogir, Croatia</h2>
              <p>Visit This Place</p>
            </a>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-12">
            <a href="#" class="place">
              <img src="asset/images/img_2.jpg" alt="Image placeholder">
              <h2>Stockton Beach, Australia</h2>
              <p>Visit This Place</p>
            </a>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-12">
            <a href="#" class="place">
              <img src="asset/images/img_2.jpg" alt="Image placeholder">
              <h2>Desert, Morocco</h2>
              <p>Visit This Place</p>
            </a>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-12">
            <a href="#" class="place">
              <img src="asset/images/img_2.jpg" alt="Image placeholder">
              <h2>Taj Mahal, India</h2>
              <p>Visit This Place</p>
            </a>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-12">
            <a href="#" class="place">
              <img src="asset/images/img_2.jpg" alt="Image placeholder">
              <h2>Eiffel Tower, France</h2>
              <p>Visit This Place</p>
            </a>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-12">
            <a href="#" class="place">
              <img src="asset/images/img_2.jpg" alt="Image placeholder">
              <h2>Opera House, Australia</h2>
              <p>Visit This Place</p>
            </a>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-12">
            <a href="#" class="place">
              <img src="asset/images/img_2.jpg" alt="Image placeholder">
              <h2>Taj Mahal, India</h2>
              <p>Visit This Place</p>
            </a>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-12">
            <a href="#" class="place">
              <img src="asset/images/img_2.jpg" alt="Image placeholder">
              <h2>Eiffel Tower, France</h2>
              <p>Visit This Place</p>
            </a>
          </div>
          <div class="col-lg-4 col-md-4 col-sm-6 col-12">
            <a href="#" class="place">
              <img src="asset/images/img_2.jpg" alt="Image placeholder">
              <h2>Opera House, Australia</h2>
              <p>Visit This Place</p>
            </a>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

    <section class="section-cover" data-stellar-background-ratio="0.5" style="background-image: url(asset/images/study-book.jpg);">
      <div class="container">
        <div class="row justify-content-center align-items-center intro">
          <div class="col-md-7 text-center element-animate">
            <h2>Kami selalu memberikan pelayanan dan penawaran terbaik</h2>
            
            <p><a href="#" class="btn btn-black">Cek Produk terbaru</a></p>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

@endsection