@extends('layout.utama')

@section('content')

	<script src="http://maps.google.com/maps/api/js"></script>
  	<script src="gmaps.js"></script>
  	<style>
      #map {
        width: 100%;
        height: 400px;
        background-color: grey;
      }
    </style>


	<section class="site-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-7 text-center">
            <h2>Lokasi Kami</h2>
          </div>
        </div>
        <div class="row">

        	<div id="map"></div>
                <script>
					// Initialize and add the map
					function initMap() {
					  // The location of Uluru
					  var uluru = {lat: -3.2421738, lng: 104.6648343};
					  // The map, centered at Uluru
					  var map = new google.maps.Map(
					      document.getElementById('map'), {zoom: 15, center: uluru});
					  // The marker, positioned at Uluru
					  var marker = new google.maps.Marker({position: uluru, map: map});
					}
				</script>
        	
        </div>
      </div>
    </section>


    <section class="site-section">
      <div class="container">
        <div class="row">
          <div class="col-lg-6 col-md-12 mb-5 element-animate">
            <h3 class="mb-5">Say "Hi", Don't be shy.</h3>
            <form action="#" method="post">
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label for="name">Name</label>
                      <input type="text" id="name" class="form-control ">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label for="phone">Phone</label>
                      <input type="text" id="phone" class="form-control ">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label for="email">Email</label>
                      <input type="email" id="email" class="form-control ">
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-12 form-group">
                      <label for="message">Write Message</label>
                      <textarea name="message" id="message" class="form-control " cols="30" rows="8"></textarea>
                    </div>
                  </div>
                  <div class="row">
                    <div class="col-md-6 form-group">
                      <input type="submit" value="Send Message" class="btn btn-primary">
                    </div>
                  </div>
                </form>
          </div>
          <div class="col-lg-1"></div>
          <div class="col-lg-5 element-animate">
            <h3 class="mb-5">Contact Info</h3>
             <ul class="list-unstyled ">
              <li class="d-flex mb-3"><span class="mr-3"><span class="icon ion-ios-location"></span></span><span class="">34 Street Name, City Name Here, United States</span></li>
              <li class="d-flex mb-3"><span class="mr-3"><span class="icon ion-ios-telephone"></span></span><span class="">+1 242 4942 290</span></li>
              <li class="d-flex mb-3"><span class="mr-3"><span class="icon ion-email"></span></span><span class="">info@yourdomain.com</span></li>
            </ul>
          </div>
          
        </div>
      </div>
    </section>
    <!-- END section -->

    <section class="section-cover" data-stellar-background-ratio="0.5" style="background-image: url(asset/images/study-book.jpg);">
      <div class="container">
        <div class="row justify-content-center align-items-center intro">
          <div class="col-md-7 text-center element-animate">
            <h2>Kami selalu memberikan pelayanan dan penawaran terbaik</h2>
            
            <p><a href="#" class="btn btn-black">Cek Produk terbaru</a></p>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

    <!-- AIzaSyDZS9KvvS9XotGirgFXtW8G8LSJYFjpp_c -->
    <script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDZS9KvvS9XotGirgFXtW8G8LSJYFjpp_c&callback=initMap">
    </script>

@endsection