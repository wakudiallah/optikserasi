@extends('layout.utama')

@section('content')

    
  <section class="site-section">
      <div class="container">
        <div class="row justify-content-center">
          <div class="col-md-7 text-center">
            <h2 class="mb-5">Tentang Kami</h2>
          </div>
        </div>
        <div class="row">
          <div class="col-lg-6 col-md-12 mb-5 element-animate" data-animate-effect="fadeInLeft">
            <img src="{{asset('asset/images/img_3.jpg')}}" class="img-md-fluid" alt="Placeholder image">
          </div>
          <div class="col-lg-6 col-md-12">
            <div class="bg-white pl-lg-5 pl-0  pb-lg-5 pb-0 element-animate" data-animate-effect="fadeInRight">
            <h2>About Us</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aspernatur atque perferendis, laudantium quod architecto velit ad officiis facere eveniet in fuga fugiat delectus rerum doloribus quos consectetur unde, expedita, quibusdam corporis impedit quia sequi aliquid sit. Ducimus labore molestias odio nam necessitatibus laboriosam vero saepe enim nobis. Repudiandae quidem, sint earum dolorum consequuntur dignissimos excepturi mollitia omnis aliquid, corporis, unde!</p>
            <p>Esse, rem, ab. Ab ipsum at eligendi aliquam veritatis vel cupiditate odio mollitia minima impedit. Explicabo dolores aut accusantium et aliquam inventore aperiam deleniti, veritatis nihil! Odit consectetur eius tempora necessitatibus possimus earum, totam pariatur. Alias id officiis atque laboriosam laudantium esse quidem est ad perspiciatis porro, non quaerat, accusamus ipsam quis adipisci, reiciendis mollitia quos sequi et nemo. Eveniet.</p>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

    <section class="site-section">
      <div class="container">
        <div class="row justify-content-center mb-5">
          <div class="col-md-7 text-center">
            <h2>Tim Kami</h2>
            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Earum magnam illum maiores adipisci pariatur, eveniet.</p>
          </div>
        </div>
        <div class="row top-destination">
          <div class="col-md-4 col-sm-6 col-12 element-animate">
            <a href="#" class="place">
              <img src="{{asset('asset/images/img_3.jpg')}}" alt="Image placeholder">
              <h2 class="text-center">Risma Handayati</h2>
              <p class="text-center">CEO</p>
            </a>
          </div>
          <div class="col-md-4 col-sm-6 col-12 element-animate">
            <a href="#" class="place">
              <img src="{{asset('asset/images/img_3.jpg')}}" alt="Image placeholder">
              <h2 class="text-center">Wakudiallah</h2>
              <p class="text-center">Marketing & Media Manager</p>
            </a>
          </div>
          <div class="col-md-4 col-sm-6 col-12 element-animate">
            <a href="#" class="place">
              <img src="{{asset('asset/images/img_3.jpg')}}" alt="Image placeholder">
              <h2 class="text-center">Novianti</h2>
              <p class="text-center">Kreative Manager</p>
            </a>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->


    <section class="section-cover" data-stellar-background-ratio="0.5" style="background-image: url(asset/images/study-book.jpg);">
      <div class="container">
        <div class="row justify-content-center align-items-center intro">
          <div class="col-md-7 text-center element-animate">
            <h2>Kami selalu memberikan pelayanan dan penawaran terbaik</h2>
            
            <p><a href="#" class="btn btn-black">Cek Produk terbaru</a></p>
          </div>
        </div>
      </div>
    </section>
    <!-- END section -->

@endsection