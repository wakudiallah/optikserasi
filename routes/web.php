<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('/', function () {
    return view('layout.utama');
});*/

Route::get('/', 'UtamaController@index');


Route::get('/utama', function () {
    return view('layout.utama');
});

Route::get('ttg', 'UtamaController@tentang');
Route::get('kacamata', 'UtamaController@kacamata');
Route::get('contactlense', 'UtamaController@contactlense');
Route::get('contact', 'UtamaController@contact');
Route::get('artikel', 'UtamaController@artikel');


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/tentang', 'HomeController@tentang');

Route::group(['middleware' => ['auth']], function() {
    //admin
	Route::resource('kacamata-admin', 'KacamataController');
	Route::post('/kacamata-post', 'Utama\MessageController@store');
	Route::resource('contactlense_admin', 'ContactLenseController');
	Route::resource('customer', 'CustomerController');
	Route::resource('admin', 'AdminController');
	Route::get('admin-index', 'AdminController@index');
	Route::get('admin-create', 'AdminController@create');
	//Route::post('admin-save', 'AdminController@store');
	Route::post('admin-save', 'RegisterController@store');
});



//ojek
Route::get('/ojek', function () {
    return view('ojek.index');
});