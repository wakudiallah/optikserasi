<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateKacamatasTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        if(!Schema::hasTable('kacamata'))
        {
            Schema::create('kacamata', function (Blueprint $table) {
                $table->increments('id');
                $table->string('title',200);
                $table->string('desc',5000);
                $table->integer('harga');
                $table->integer('hargadummy');
                $table->SoftDeletes();
                $table->timestamps();
            });
        }
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kacamata');
    }
}
